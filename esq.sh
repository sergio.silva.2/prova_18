#!/bin/bash
clear
Menu() {
    clear
    echo -e '\n'
    echo "=========================="
    echo "       MENU OPÇÕES        "
    echo "=========================="
    echo "__________________________"
    echo "[ a ] | opção a"
    echo "[ b ] | Opção b"
    echo "[ c ] | Opção c"
    echo "[ d ] | Opção d"
    echo "[ e ] | Opção e"
    echo "[ f ] | Opção f"
    echo "[ g ] | SAIR"
    echo "[ 0 ] | SAIR"
    echo -e '\n'
    echo "RESPOSTA: "
    read opcao
    case $opcao in
    a) Opcaoa ;;
    b) Opcaob ;;
    c) Opcaoc ;;
    d) Opcaod ;;
    e) Opcaoe ;;
    f) Opcaof ;;
    g) Opcaog ;;
    #0) Sair ;;#/
    *) "Comando desconhecido" ; echo ; Menu;;
    esac
    }

Opcaoa () {
    clear
    echo "selecionar um arquivo"
    echo "cd lerolero.txt /home/ifpb"
    echo "[ 1 ] Voltar"
    echo "[ 0 ] Sair"
    read opcao
    case $opcao in
    1) Voltar ;;
    0) Sair ;;
    *) "Comando desconhecido" ; echo ; Opcaoa ;;
    esac
}

Opcaob () {
    clear
    echo "fazer um preview do arquivo selecionado"
    echo "head -2 lerolero.txt"
    echo "[ 1 ] Voltar"
    echo "[ 0 ] Sair"
    read opcao
    case $opcao in
    1) Voltar ;;
    0) Sair ;;
    *) "Comando desconhecido" ; echo ; Opcaob  ;;
    esac
}

Opcaoc () {
    clear
    echo "exibir o arquivo selecionado"
    echo grep -v "upgrade" lerolero.txt
    echo "[ 1 ] Voltar"
    echo "[ 0 ] Sair"
    read opcao
    case $opcao in
    1) Voltar ;;
    0) Sair ;;
    *) "Comando desconhecido" ; echo ; Opcaoc  ;;
    esac
}

Opcaod () {
    clear
    echo "criar um arquivo com n linhas de blablabla"
    echo "head -2 lerolero.txt > novolerolero.txt"
    echo "[ 1 ] Voltar"
    echo "[ 0 ] Sair"
    read opcao
    case $opcao in
    1) Voltar ;;
    0) Sair ;;
    *) "Comando desconhecido" ; echo ; Opcaod  ;;
    esac
}

Opcaoe () {
    clear
    echo "pesquisar algo no arquivo selecionado"
    echo ""
    echo "[ 1 ] Voltar"
    echo "[ 0 ] Sair"
    read opcao
    case $opcao in
    1) Voltar ;;
    0) Sair ;;
    *) "Comando desconhecido" ; echo ; Opcaoe  ;;
    esac
}

Opcaof () {
    clear
    echo "criar uma cópia do arquivo selecionado"
    echo ""
    echo "[ 1 ] Voltar"
    echo "[ 0 ] Sair"
    read opcao
    case $opcao in
    1) Voltar ;;
    0) Sair ;;
    *) "Comando desconhecido" ; echo ; Opcaof  ;;
    esac
}

Opcaog () {
    clear
    echo "SAIR"
    echo "[ 1 ] Voltar"
    echo "[ 0 ] Sair"
    read opcao
    case $opcao in
    1) Voltar ;;
    0) Sair ;;
    *) "Comando desconhecido" ; echo ; Opcaog  ;;
    esac}

Voltar() {
    clear
        Menu
}

Sair() {
    clear
    exit
}
clear
Menu
